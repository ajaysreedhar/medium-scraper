'use strict';

/**
 * Extracts the value of href attribute from a given anchor tag.
 *
 * @private
 * @param anchor input anchor tag
 * @returns {String} value of the href attribute
 */
var _href = function (anchor) {
    if (typeof anchor !== 'string' || anchor.length <= 0) return '';

    var split = anchor.replace('<a ', '').replace('>', '').split(/(href)\s*=\s*/g), link, end = 1;

    for (var index = 0; index < split.length; index++) {
        if (split[index] === 'href' && typeof split[index + 1] === 'string') {
            link = split[index + 1];

            if(link.charAt(0) !== '"') return '';

            end = 1;
            while (link.charAt(end) !== '"' && link.charAt(end) !== '\0') {
                end++;
            }

            link = link.substring(1, end).split('?')[0];

            if (link.indexOf('https://medium.com') >= 0) {
                return link;
            }
        }
    }

    return '';
};

/**
 * Parses the entire HTML text and finds all the anchor tags present in the text.
 *
 * @param html input HTML text
 * @returns {Array} array of href values found
 */
var Parse = function (html) {
    var length = html.length, start = 0, index = 0;
    var opened = false;
    var href = '';
    var links = [];

    if (length <= 0) return links;

    while (index < length) {
        if (html.charAt(index) === '<') {
            start = index;

        } else if (html.charAt(index) === 'a' && html.charAt(index + 1) === ' ') {
            opened = true;

        } else if (opened && html.charAt(index) === '>') {
            href = _href(html.substring(start, index + 1));
            if (href.length > 0) links.push(href);

            opened = false;
            start = index;
        }

        index++;
    }

    return links;
};

module.exports = {
    Parse: Parse
};
