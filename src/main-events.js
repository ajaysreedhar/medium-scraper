/**
 * main-events.js
 *
 * Implementation of the medium-scraper application
 * without using async library.
 */
'use strict';

(function (process) {
    const events = require('events');
    const unirest = require('unirest');
    const files = require('fs');
    const _ = require('underscore');

    const hyperlinks = require('./hyperlinks');

    var emitter = new events.EventEmitter();

    /**
     * Name of the output file. Defaults to output.csv.
     *
     * @type {string}
     */
    var csv = (typeof process.argv[2] === 'string' ? process.argv[2] : 'output.csv');

    /**
     * Queue of links found so far.
     *
     * Initialized with https://medium.com.
     *
     * @type {Array}
     */
    var links = ['https://medium.com/'];

    /**
     * Number of concurrent requests.
     *
     * Has a minimum value of 0 and maximum value of 5.
     * Incremented by 1 when a new request is fired
     * and decremented by 1 when a request completes.
     *
     * @type {number}
     */
    var requests = 0;

    /**
     * Index of current URL being crawled in the queue.
     *
     * @type {number}
     */
    var index = 0;

    process.on('SIGINT', () => {
        console.log('\nAborted!');
        process.exit(0);
    });

    /**
     * Empty contents of the file if already exists.
     */
    files.exists(process.cwd() + '/' + csv, () => {
        files.writeFile(process.cwd() + '/' + csv, '', {}, (error) => {
            if(error) console.error('ERROR: ' + error);
        });
    });

    emitter.on('next', () => {
        while (requests < 5) {
            /* Return if next link is not available yet. */
            if (typeof links[index] === 'undefined') {
                return;
            }

            requests++;

            console.log('Crawling ' + links[index]);

            unirest.get(links[index])
                .headers({'Accept': 'text/html'})
                .end((response) => {
                    requests--;

                    if (typeof response.raw_body !== 'string') {
                        return;
                    }

                    var parsed = hyperlinks.Parse(response.raw_body);

                    /* Iterate over parsed links and add to the queue if the link does not exist. */
                    for (var index = 0; index < parsed.length; index++) {

                        /* Skip if the link already exists in the queue. */
                        if (_.contains(links, parsed[index])) {
                            continue;
                        }

                        links.push(parsed[index]);

                        /* Write to file. */
                        files.appendFile(process.cwd() + '/' + csv, parsed[index] + '\n', {}, (error) => {
                            if (error) console.error('ERROR: ' + error);
                        });
                    }

                    /* Terminate if this was the last request. */
                    if (links[index] === 'undefined') {
                        console.log('Done.');
                        process.exit(0);
                    }

                    /* Emit the event to crawl next URL. */
                    emitter.emit('next');
                });

            index++;
        }
    });

    /* Initiate crawling. */
    emitter.emit('next');

})(process);