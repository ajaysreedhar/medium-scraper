'use strict';

/**
 * main-async.js
 *
 * Implementation of the medium-scraper application
 * using async library.
 */
(function (process) {
    const async = require('async');
    const unirest = require('unirest');
    const files = require('fs');
    const _ = require('underscore');

    const hyperlinks = require('./hyperlinks');

    /**
     * Name of the output file. Defaults to output.csv.
     *
     * @type {string}
     */
    var csv = (typeof process.argv[2] === 'string' ? process.argv[2] : 'output.csv');

    /**
     * Queue of links found so far.
     *
     * Initialized with https://medium.com.
     *
     * @type {Array}
     */
    var links = ['https://medium.com/'];

    /**
     * Index of current URL being crawled in the queue.
     *
     * @type {number}
     */
    var index = 0;

    /**
     * Number of concurrent requests.
     *
     * Has a minimum value of 0 and maximum value of 5.
     * Incremented by 1 when a new request is fired
     * and decremented by 1 when a request finishes crawling.
     *
     * @type {number}
     */
    var requests = 0;

    process.on('SIGINT', () => {
        console.log('\nAborted!');
        process.exit(0);
    });

    /**
     * Empty contents of the file if already exists.
     */
    files.exists(process.cwd() + '/' + csv, () => {
        files.writeFile(process.cwd() + '/' + csv, '', {}, (error) => {
            if (error) console.error('ERROR: ' + error);
        });
    });

    var queue = async.queue((url, callback) => {
        console.log('Crawling ' + url);

        unirest.get(url)
            .headers({'Accept': 'text/html'})
            .end((response) => {
                requests--;

                if (typeof response.raw_body !== 'string') {
                    return;
                }

                var parsed = hyperlinks.Parse(response.raw_body);

                /* Iterate over parsed links and add to the queue
                 * if the link does not exist. */
                for (var i = 0; i < parsed.length; i++) {

                    /* Skip if the link already exists in the queue. */
                    if (_.contains(links, parsed[i])) {
                        continue;
                    }

                    links.push(parsed[i]);

                    /* Write to file. */
                    files.appendFile(process.cwd() + '/' + csv, parsed[i] + '\n', {}, (error) => {
                        if (error) console.error('ERROR: ' + error);
                    });
                }

                /* Fire next requests by adding it to async queue.
                 * Add till concurrency reaches 5. */
                while (requests < 5) {
                    index++;

                    if (typeof links[index] !== 'string') {
                        return;
                    }

                    queue.push(links[index], (error) => {
                        if (error) console.error('ERROR: ' + error);
                    });

                    requests++;
                }
            });

        if (typeof callback === 'function') {
            callback();
        }

    }, 5);

    /* Start by pushing the first URL to the queue. */
    queue.push(links[0]);

})(process);