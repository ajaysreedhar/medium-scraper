## Medium Scraper

A web scraper to crawl popular blogging website [Medium](https://medium.com/) and find all possible hyperlinks
present within ​the​ website and generate a CSV output.

This repo contains two versions of the same application, one implemented using the __async__ library and the other without.

#### Installing

Clone the repository:
```bash
$ git clone https://bitbucket.org/ajaysreedhar/medium-scraper
```

Install dependencies:
```bash
$ cd medium-scraper
$ npm install
```

#### Run the crawler

Without using async library:
```bash
$ npm run crawl-events
```

Using async library:
```bash
$ npm run crawl-async
```

#### (Optional) Run ESLint
```bash
$ npm test
```